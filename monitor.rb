#!/usr/bin/env ruby
require 'dotenv/load'
require 'watir'
require 'pry'
require 'twilio-ruby'

require './pages/pages.rb'

require 'postmark'

class Grabber
  attr_accessor :reserved
  attr_accessor :url

  def initialize(url)
    # Create an instance of Postmark::ApiClient:
    @postmark = Postmark::ApiClient.new(ENV['POSTMARK_API_KEY'])

    @url = url
    @browser = Watir::Browser.new
    @reserved = false

    @site = Site.new(@browser)
    binding.pry if ENV['DEBUG']
  end

  def visit
    browser.goto @url
  end

  def start
    puts "Starting scanning for listings..."

    while !reserved?
      check_for_available
    end

    puts "Reserved done!"
  end

  def browser
    @browser
  end

  def site
    @site
  end

  def reserved?
    @reserved == true
  end

  def check_for_available
    puts "Refreshing browser window."
    ts = site.ticketswap.open

    puts "Checking if signed in..."
    handle_signed_out unless ts.signed_in?

    puts "Getting listings"
    listings = get_active_listings

    puts "Found #{listings.size} listings"

    if listings.any?
      puts "Listing found! Woohoo!"
      handle_found(listings)
    else
      puts "None available yet, waiting for 5 seconds to try again..."
      sleep 5
    end
  end

  def handle_signed_out
    puts "Wasn't signed in, signing in..."
    # Create a FB session first
    site.facebook.sign_in

    # Go back to TS
    ts = site.ticketswap.open

    # Sign in on ticketswap now that we have a FB session
    site.ticketswap.sign_in
    browser.wait

    # Just in case we check if we are still on the event page
    ts = site.ticketswap.open unless browser.url == ENV['EVENT_URL']
    ts
  end

  def handle_found(listings = nil)
    listings = listings || get_active_listings
    listings.first.click

    too_late = browser.div(class: 'too-late--info')
    unavailable = browser.div(class: 'listing-unavailable')
    return puts "Oh no, too late anyway" if too_late.exists?
    return puts "Oh no, too late anyway" if unavailable.exists?

    ticket_amount_select = browser.select id: "listing-show-amount"

    if ticket_amount_select.options.map(&:value).include?("2")
      ticket_amount_select.select '2 tickets'
    else
      ticket_amount_select.select '1 ticket'
    end

    submit_button = browser.input class: 'btn-buy'
    submit_button.click

    payment_cart =  browser.div(class: 'cart-payment')
    sleep 4

    if payment_cart.exists?
      send_mail(message: "Ella re, check snel je winkelwagen op ticketswap, er zit een kaartje in!", subject: "Lowlands kaartje gevonden!!!!!!")
      `say "I found a ticket! Check your cart!"`
      return @reserved = true
    else
      raise "Fout! Je bent niet ingelogd op facebook of zo"
    end
  end

  def get_active_listings
    articles = browser.articles(class: 'listings-item')
    articles.reject { |a| a.class_name.include?('listings-item--not-for-sale') }
  end

  def send_mail(message:, subject:)
    # Send an email:
    @postmark.deliver(
      from: 'info@abuisman.nl',
      to: ENV['MAIL_TO'],
      subject: subject,
      html_body: message,
      track_opens: true)
  end

  def make_call
    twilio_url = "https://gist.githubusercontent.com/abuisman/c3019cf10a755b9f44fc2bbf737bbc96/raw/b4bfdeba78e2a4dcaddb730096a1f552c1c1f2cf/added-to-cart.xml"

    @client = Twilio::REST::Client.new ENV['TWILIO_API_SID'], ENV['TWILIO_API_SECRET']
    call = @client.api.account.calls.create(:url => twilio_url, :to => ENV['CALL_TO'], :from => ENV['TWILIO_FROM_NUMBER'])
  end
end

grabber = Grabber.new(ENV['EVENT_URL'])

def start_grabber(grabber)
  begin
    grabber.start
  rescue
    puts "\n\n"
    puts "Some error happened, retrying"
    start_grabber(grabber)
    puts "\n\n"
  end
end

start_grabber(grabber)
