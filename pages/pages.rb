class BrowserContainer
  def initialize(browser)
    @browser = browser
  end

  def browser
    @browser
  end
end

class Site < BrowserContainer
  def facebook
    @facebook = Facebook.new(browser)
  end

  def ticketswap
    @ticketswap = TicketSwap.new(browser)
  end
end

class Facebook < BrowserContainer
  def open
    browser.goto "https://www.facebook.com/"
    self
  end

  def signed_in?
    !email_field.exists?
  end

  def sign_in
    open
    browser.wait

    return true if signed_in?

    email_field.set ENV['FACEBOOK_EMAIL']
    password_field.set ENV['FACEBOOK_PASSWORD']
    login_button.click
  end

  # private

  def login_button
    browser.element id: 'loginbutton'
  end

  def email_field
    browser.text_field id: 'email'
  end

  def password_field
    browser.text_field id: 'pass'
  end
end

class TicketSwap < BrowserContainer
  URL = ENV['EVENT_URL']

  def open
    browser.goto URL
    self
  end

  def sign_in
    open
    return true if signed_in?

    browser.goto "https://www.ticketswap.nl/login"
    button = browser.element class: 'btn-lg'
    button.click

    # Sleeping 2 seconds just in case
    sleep 2
  end

  def signed_in?
    !signed_in_element.exists?
  end

  def signed_in_element
    browser.link(css: '[data-facebook-login]')
  end
end
